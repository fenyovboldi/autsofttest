# AutSoftTest

A projekt a NY Times Most Popular cikkjeit kéri le és jeleníti meg egy listában,<br>
valamint a cikk részleteit is meg lehet tekinteni.<br>

Checkout után nem szükséges további lépés, hogy lefordítható legyen<br>

Dependencyk:<br>
    -SupportLibraries<br>
    -Gson<br>
    -Room<br>
    -RxJava2<br>
    -Picasso<br>
    -Retrofit<br>
    -Koin<br>
    -LiveData<br>
    -Customtabs<br>
    
A perzisztens adattárolást Room-al készítettem el.<br>
Koin dependency injection framework-t használtam az MVVM architektúra elkészítéséhez.<br>
Képbetöltéshez Picasso-t használtam.<br>
A ViewModel és View-k közötti kommunkációhoz LiveData-t használtam.<br>
A network call-okat Retrofittel, Rxjava2 adapterrel és GsonConverterel valósítottam meg.<br>

