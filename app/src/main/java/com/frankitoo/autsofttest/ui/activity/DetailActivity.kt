package com.frankitoo.autsofttest.ui.activity

import android.content.Intent
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.frankitoo.autsofttest.R
import com.frankitoo.autsofttest.repository.News.local.NewsEntity
import com.frankitoo.autsofttest.ui.base.BaseActivity
import com.frankitoo.autsofttest.ui.util.ImageHelper
import com.frankitoo.autsofttest.ui.util.NetworkHelper
import com.frankitoo.autsofttest.ui.util.WindowHelper
import com.frankitoo.autsofttest.ui.viewmodel.News.NewsDetailUIModel
import com.frankitoo.autsofttest.ui.viewmodel.News.NewsDetailViewModel
import kotlinx.android.synthetic.main.activity_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel


fun startDetailActivity(newsId: Long?, activity: BaseActivity) {
    val intent = Intent(activity, DetailActivity::class.java)
    newsId?.let {
        intent.putExtra(DetailActivity.KEY_NEWS_ID, it)
    }
    activity.startActivity(intent)
}

class DetailActivity : BaseActivity() {

    companion object {
        const val KEY_NEWS_ID = "key_news_id"
    }

    val newsDetailModel: NewsDetailViewModel by viewModel()

    var newsId: Long? = null

    override fun getContentView() = R.layout.activity_detail

    override fun initUi() {
        initDefaults()

        if (newsId != null) {
            newsDetailModel.getNewsDetail(newsId!!)
        }

        newsDetailModel.uiData.observe(this, Observer<NewsDetailUIModel> {
            if (it.newsEntity != null) {
                displayByNewsEntity(it.newsEntity)
            } else if (it.error != null) {
                showSnack(it.error.message)
            }
        })
    }

    private fun initDefaults() {
        btnBack.setOnClickListener {
            finish()
        }

        btnChrome.isEnabled = false

        val extras = intent.extras
        if (extras != null && !extras.isEmpty) {
            newsId = extras.getLong(KEY_NEWS_ID)
        }
    }

    private fun displayByNewsEntity(newsEntity: NewsEntity) {

        //Set TextViews
        tvTitle.text = newsEntity.title ?: ""
        tvByLine.text = newsEntity.byline ?: ""
        tvDate.text = newsEntity.date ?: ""

        //Load image with Picasso
        val widthpx = WindowHelper.getScreenWidth(this)
        val heightpx = WindowHelper.pxFromDp(this, 240)

        ImageHelper.loadImageToView(newsEntity.imageUrl, imageNews, widthpx, heightpx)

        //Handle chrome button
        btnChrome.isEnabled = (newsEntity.url != null)
        btnChrome.setOnClickListener {
            if (NetworkHelper.checkNetworkConnection(this)) {
                startChrome(newsEntity.url!!)
            }
        }
    }

    private fun startChrome(url: String) {
        val builder = CustomTabsIntent.Builder()
        builder.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary))
        val customTabsIntent = builder.build()
        customTabsIntent.launchUrl(this, Uri.parse(url))
    }

}