package com.frankitoo.autsofttest.ui.viewmodel.News

import androidx.lifecycle.MutableLiveData
import com.frankitoo.autsofttest.repository.News.NewsRepository
import com.frankitoo.autsofttest.repository.News.local.NewsEntity
import com.frankitoo.autsofttest.ui.viewmodel.AbstractViewModel
import com.frankitoo.autsofttest.util.ext.with
import com.frankitoo.autsofttest.util.rx.SchedulerProvider

class NewsViewModel(private val newsRepository: NewsRepository, private val scheduler: SchedulerProvider) :
    AbstractViewModel() {

    val uiData = MutableLiveData<NewsUIModel>()

    fun getNews() {
        launch {
            newsRepository.getNews().with(scheduler)
                .subscribe({ list ->
                    uiData.value = NewsUIModel(list)
                }, { e ->
                    uiData.value = NewsUIModel(error = e)
                })
        }
    }

    fun refreshNews() {
        newsRepository.refreshNews()
    }

    override fun onCleared() {
        super.onCleared()
        newsRepository.onClear()
    }
}

data class NewsUIModel(val list: List<NewsEntity> = emptyList(), val error: Throwable? = null)