package com.frankitoo.autsofttest.ui.view

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.frankitoo.autsofttest.ui.adapter.RecyclerViewEmptySupportAdapter

open class RecyclerViewEmptySupport(context: Context, attrs: AttributeSet?) : RecyclerView(context, attrs) {

    var requiredLayoutManager: LayoutManager? = null
        set(value) {
            field = value
            layoutManager = requiredLayoutManager
        }

    override fun setAdapter(adapter: Adapter<*>?) {
        super.setAdapter(adapter)

        adapter?.registerAdapterDataObserver(emptyObserver)

        emptyObserver.onChanged()
    }

    private val emptyObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onChanged() {
            val adapter = adapter

            if (adapter != null && adapter is RecyclerViewEmptySupportAdapter) {
                layoutManager = if (adapter.isEmpty) {
                    LinearLayoutManager(context)
                } else {
                    requiredLayoutManager
                }
            }
        }
    }

}