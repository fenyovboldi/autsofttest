package com.frankitoo.autsofttest.ui.viewmodel.News

import androidx.lifecycle.MutableLiveData
import com.frankitoo.autsofttest.repository.News.NewsRepository
import com.frankitoo.autsofttest.repository.News.local.NewsEntity
import com.frankitoo.autsofttest.ui.viewmodel.AbstractViewModel
import com.frankitoo.autsofttest.util.ext.with
import com.frankitoo.autsofttest.util.rx.SchedulerProvider
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class NewsDetailViewModel(private val newsRepository: NewsRepository, private val scheduler: SchedulerProvider) :
    AbstractViewModel() {

    val uiData = MutableLiveData<NewsDetailUIModel>()

    fun getNewsDetail(id: Long) {
        GlobalScope.launch {
            val news = newsRepository.getNewsById(id)
            uiData.value = NewsDetailUIModel(news)
            //uiData.value = NewsDetailUIModel(error = e)
        }
    }

}

data class NewsDetailUIModel(val newsEntity: NewsEntity? = null, val error: Throwable? = null)
