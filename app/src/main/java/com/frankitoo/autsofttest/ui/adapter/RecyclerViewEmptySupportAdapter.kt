package com.frankitoo.autsofttest.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class RecyclerViewEmptySupportAdapter(private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val VIEW_TYPE_EMPTY = 1
    private val VIEW_TYPE_NORMAL = 0

    open var emptyViewRes: Int? = null
        set(value) {
            value?.let {
                emptyView = getViewFromRes(value, null)
            }
        }

    var emptyView: View? = null
        set(value) {
            field = value
            field?.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            notifyDataSetChanged()
        }

    var isEmpty = false

    abstract fun getDataCount(): Int
    abstract fun getViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder
    abstract fun onBindVh(holder: RecyclerView.ViewHolder?, position: Int)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_EMPTY && emptyView != null) {
            EmptyViewHolder(emptyView!!)
        } else {
            getViewHolder(parent, viewType)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder !is EmptyViewHolder) {
            onBindVh(holder, position)
        }
    }

    override fun getItemCount(): Int {
        val count = getDataCount()

        isEmpty = count == 0 && emptyView != null

        return if (isEmpty) {
            1
        } else {
            count
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (isEmpty) {
            VIEW_TYPE_EMPTY
        } else {
            VIEW_TYPE_NORMAL
        }
    }

    protected fun getViewFromRes(res: Int, parent: ViewGroup?): View {
        return if (parent == null) {
            LayoutInflater.from(context).inflate(res, parent, false)
        } else {
            LayoutInflater.from(parent.context).inflate(res, parent, false)
        }
    }

    inner class EmptyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //nothing here
    }

}