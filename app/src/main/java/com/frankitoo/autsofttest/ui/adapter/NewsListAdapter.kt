package com.frankitoo.autsofttest.ui.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.frankitoo.autsofttest.R
import com.frankitoo.autsofttest.repository.News.local.NewsEntity
import com.frankitoo.autsofttest.ui.util.ImageHelper

class NewsListAdapter(context: Context) : RecyclerViewEmptySupportAdapter(context) {

    var itemSelectedListener: ((Long?) -> Unit)? = null

    var dataItems: Array<NewsEntity>? = null
        set(value) {
            value?.let {
                if (value.isNotEmpty()) {
                    field = value
                    notifyDataSetChanged()
                }
            }
        }

    override fun getViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        val view = getViewFromRes(R.layout.view_news_item, parent)
        return ViewHolder(view)
    }

    override fun onBindVh(holder: RecyclerView.ViewHolder?, position: Int) {
        if (holder is ViewHolder) {
            holder.bindData(dataItems!![position])
        }
    }

    override fun getDataCount() = dataItems?.size ?: 0

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)
        private val tvByLine = itemView.findViewById<TextView>(R.id.tvByLine)
        private val tvDate = itemView.findViewById<TextView>(R.id.tvDate)
        private val imgView = itemView.findViewById<ImageView>(R.id.img)

        fun bindData(data: NewsEntity) {
            tvTitle.text = data.title
            tvByLine.text = data.byline
            tvDate.text = data.date

            ImageHelper.loadWithRoundedFitImageToView(data.imageUrl, imgView)

            itemView.setOnClickListener {
                itemSelectedListener?.invoke(data.id)
            }
        }
    }
}