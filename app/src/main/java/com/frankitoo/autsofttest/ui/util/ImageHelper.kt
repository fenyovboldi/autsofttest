package com.frankitoo.autsofttest.ui.util

import android.widget.ImageView
import com.frankitoo.autsofttest.R
import com.squareup.picasso.Picasso

object ImageHelper {

    fun loadWithRoundedFitImageToView(url: String?, imageView: ImageView?) {
        imageView ?: return
        if (url != null) {
            Picasso.get().load(url)
                .placeholder(R.drawable.gray_circle)
                .transform(CircleTransform())
                .fit().into(imageView)
        }
    }

    fun loadImageToView(url: String?, imageView: ImageView?, width: Int, height: Int) {

        imageView ?: return

        if (url != null) {
            Picasso.get()
                .load(url)
                .resize(width, height)
                .centerCrop()
                .into(imageView)
        }
    }


}