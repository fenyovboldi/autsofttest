package com.frankitoo.autsofttest.ui.util

import android.app.Activity
import android.content.Context
import android.util.DisplayMetrics

object WindowHelper {

    private val displayMetrics = DisplayMetrics()

    fun getScreenWidth(activity: Activity): Int {
        return getMetrics(activity).widthPixels
    }

    fun pxFromDp(context: Context, dp: Int): Int {
        return (dp * context.resources.displayMetrics.density).toInt()
    }

    private fun getMetrics(activity: Activity): DisplayMetrics {
        activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics
    }

}