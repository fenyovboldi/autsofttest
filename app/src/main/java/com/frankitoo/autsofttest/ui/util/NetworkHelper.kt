package com.frankitoo.autsofttest.ui.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.widget.Toast
import com.frankitoo.autsofttest.R
import com.frankitoo.autsofttest.model.NetworkStatus
import com.frankitoo.autsofttest.ui.base.BaseActivity

object NetworkHelper {

    private fun getNetworkStatus(context: Context): NetworkStatus {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        return if (isConnected) {
            if (activeNetwork?.type == ConnectivityManager.TYPE_WIFI) NetworkStatus.NETWORK_WIFI else NetworkStatus.NETWORK_MOBILE
        } else NetworkStatus.NETWORK_NO_CONNECTION
    }

    fun checkNetworkConnection(context: Context): Boolean {
        if (getNetworkStatus(context) == NetworkStatus.NETWORK_NO_CONNECTION) {
            val errorText = context.getString(R.string.no_internet)
            if (context is BaseActivity) {
                context.showSnack(errorText)
            } else {
                Toast.makeText(context, errorText, Toast.LENGTH_SHORT).show()
            }
            return false
        }
        return true
    }
}