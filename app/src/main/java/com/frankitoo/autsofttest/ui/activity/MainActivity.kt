package com.frankitoo.autsofttest.ui.activity

import androidx.lifecycle.Observer
import com.frankitoo.autsofttest.R
import com.frankitoo.autsofttest.repository.StringResource
import com.frankitoo.autsofttest.ui.base.BaseActivity
import com.frankitoo.autsofttest.ui.util.NetworkHelper.checkNetworkConnection
import com.frankitoo.autsofttest.ui.viewmodel.News.NewsUIModel
import com.frankitoo.autsofttest.ui.viewmodel.News.NewsViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity() {

    val newsModel: NewsViewModel by viewModel()

    val stringResource : StringResource by inject()

    override fun getContentView() = R.layout.activity_main

    override fun initUi() {
        title = stringResource.getStringFromActivity()

        initSwipeRefreshLayout()
        initNews()

        newsRecycler.itemSelectedListener = {
            startDetailActivity(it, this)
        }

        newsModel.uiData.observe(this, Observer<NewsUIModel> {
            layoutSwipeRefresh.isRefreshing = false
            if (it != null) {
                val newsList = it.list
                if (newsList.isNotEmpty()) {
                    newsRecycler.items = it.list.toTypedArray()
                } else if (it.error != null) {
                    showSnack(it.error.message)
                }
            }
        })

    }

    private fun initNews() {
        newsModel.getNews()
        if (!checkNetworkConnection(this)) return
        newsModel.refreshNews()
    }

    private fun initSwipeRefreshLayout() {
        layoutSwipeRefresh.setOnRefreshListener {
            if (checkNetworkConnection(this)) {
                newsModel.refreshNews()
            } else {
                layoutSwipeRefresh.isRefreshing = false
            }
        }
    }
}
