package com.frankitoo.autsofttest.ui.view

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.frankitoo.autsofttest.R
import com.frankitoo.autsofttest.repository.News.local.NewsEntity
import com.frankitoo.autsofttest.ui.adapter.NewsListAdapter

class NewsRecycler(context: Context, attrs: AttributeSet?) : RecyclerViewEmptySupport(context, attrs) {

    private val newsListAdapter = NewsListAdapter(context)

    var itemSelectedListener: ((Long?) -> Unit)? = null
        set(value) {
            field = value
            newsListAdapter.itemSelectedListener = itemSelectedListener
        }

    var items: Array<NewsEntity>? = null
        set(value) {
            field = value
            newsListAdapter.dataItems = items
        }

    init {
        requiredLayoutManager = LinearLayoutManager(context)
        adapter = newsListAdapter
        newsListAdapter.emptyViewRes = R.layout.view_news_recycler_empty

        (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
    }

}