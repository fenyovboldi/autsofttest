package com.frankitoo.autsofttest.di

import com.frankitoo.autsofttest.repository.News.NewsRepository
import com.frankitoo.autsofttest.repository.News.NewsRepositoryImpl
import com.frankitoo.autsofttest.ui.viewmodel.News.NewsDetailViewModel
import com.frankitoo.autsofttest.ui.viewmodel.News.NewsViewModel
import com.frankitoo.autsofttest.util.rx.ApplicationSchedulerProvider
import com.frankitoo.autsofttest.util.rx.SchedulerProvider
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val newsModule = module {

    single<NewsRepository> { NewsRepositoryImpl(get(), get(), get()) }

    viewModel { NewsViewModel(get(), get()) }

    viewModel { NewsDetailViewModel(get(), get()) }
}

val rxModule = module {
    single { ApplicationSchedulerProvider() as SchedulerProvider }
}
