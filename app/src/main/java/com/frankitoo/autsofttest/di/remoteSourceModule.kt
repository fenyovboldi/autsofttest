package com.frankitoo.autsofttest.di

import com.frankitoo.autsofttest.di.DatasourceProperties.SERVER_URL
import com.frankitoo.autsofttest.repository.News.NewsDto
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val remoteSourceModule = module {
    // provided web components
    single { createOkHttpClient() }

    // provided web service
    single { createWebService<NewsDto>(get(), SERVER_URL) }
}

object DatasourceProperties {
    const val SERVER_URL = "http://api.nytimes.com/svc/mostpopular/v2/mostviewed/"
    const val API_KEY = "9vn8aJKGfIyAGRQ6b1SQGASgjLxMXsk3"
}

fun createOkHttpClient(): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
    return OkHttpClient.Builder()
        .connectTimeout(60L, TimeUnit.SECONDS)
        .readTimeout(60L, TimeUnit.SECONDS)
        .addInterceptor(httpLoggingInterceptor).build()
}

inline fun <reified T> createWebService(okHttpClient: OkHttpClient, url: String): T {
    val retrofit = Retrofit.Builder()
        .baseUrl(url)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
    return retrofit.create(T::class.java)
}