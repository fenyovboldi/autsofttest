package com.frankitoo.autsofttest.di

import com.frankitoo.autsofttest.repository.StringResource
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val stringResourceModule = module {
    single { StringResource(androidApplication())}
}