package com.frankitoo.autsofttest.di

import androidx.room.Room
import com.frankitoo.autsofttest.repository.NewsDataBase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val localSourceModule = module {
    // provided room components
    single {
        Room.databaseBuilder(androidApplication(), NewsDataBase::class.java, "news-db")
            .fallbackToDestructiveMigration()
            .build()
    }

    // provided news dao
    single{
        get<NewsDataBase>().newsDao()
    }

}