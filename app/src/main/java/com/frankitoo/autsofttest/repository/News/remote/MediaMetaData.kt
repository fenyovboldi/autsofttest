package com.frankitoo.autsofttest.repository.News.remote

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class MediaMetaData(
    @SerializedName("url")
    @Expose var url: String? = null,
    @SerializedName("format")
    @Expose var format: String? = null,
    @SerializedName("height")
    @Expose var height: Int? = null,
    @SerializedName("width")
    @Expose var width: Int? = null
)