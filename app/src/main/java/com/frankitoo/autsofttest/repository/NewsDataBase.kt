package com.frankitoo.autsofttest.repository

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.frankitoo.autsofttest.repository.News.local.NewsDao
import com.frankitoo.autsofttest.repository.News.local.NewsEntity

@Database(entities = arrayOf(NewsEntity::class), version = 3)
abstract class NewsDataBase : RoomDatabase() {
    abstract fun newsDao(): NewsDao
}