package com.frankitoo.autsofttest.repository.News

import com.frankitoo.autsofttest.di.DatasourceProperties
import com.frankitoo.autsofttest.repository.News.remote.NewsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface NewsDto {

    @GET("all-sections/7.json?")
    @Headers("Content-type: application/json")
    fun getNews(
        @Query("api-key") apiKey: String = DatasourceProperties.API_KEY
        ): Single<NewsResponse>

}