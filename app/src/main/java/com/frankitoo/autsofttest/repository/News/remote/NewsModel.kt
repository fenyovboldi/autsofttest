package com.frankitoo.autsofttest.repository.News.remote

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class NewsModel(
    @SerializedName("id")
    @Expose var id: Long? = null,
    @SerializedName("url")
    @Expose var url: String? = null,
    @SerializedName("byline")
    @Expose var byline: String? = null,
    @SerializedName("title")
    @Expose var title: String? = null,
    @SerializedName("published_date")
    @Expose var date: String? = null,
    @SerializedName("media")
    @Expose var media: List<NewsMediaModel>? = null
)