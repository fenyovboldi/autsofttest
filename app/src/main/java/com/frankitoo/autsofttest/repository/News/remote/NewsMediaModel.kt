package com.frankitoo.autsofttest.repository.News.remote

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class NewsMediaModel(
    @SerializedName("type")
    @Expose var type: String? = null,
    @SerializedName("media-metadata")
    @Expose var mediaMetaData: List<MediaMetaData>? = null
)