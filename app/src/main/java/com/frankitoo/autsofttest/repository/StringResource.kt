package com.frankitoo.autsofttest.repository

import android.content.Context
import android.widget.Toast
import com.frankitoo.autsofttest.R
import com.frankitoo.autsofttest.ui.base.BaseActivity

class StringResource(val context : Context) {
    fun getStringFromActivity(): String? {
        return context.getString(R.string.my_string)
    }

    fun showToast(context: Context) {
        Toast.makeText(context, "From Toast", Toast.LENGTH_LONG).show()
    }
}