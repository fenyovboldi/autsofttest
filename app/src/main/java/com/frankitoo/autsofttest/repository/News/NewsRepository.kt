package com.frankitoo.autsofttest.repository.News

import android.util.Log
import com.frankitoo.autsofttest.repository.News.local.NewsDao
import com.frankitoo.autsofttest.repository.News.local.NewsEntity
import com.frankitoo.autsofttest.util.ext.with
import com.frankitoo.autsofttest.util.rx.SchedulerProvider
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import retrofit2.HttpException

interface NewsRepository {
    fun getNews(): Observable<List<NewsEntity>>
    suspend fun getNewsById(id: Long): NewsEntity
    fun refreshNews()
    fun onClear()
}

class NewsRepositoryImpl(
    private val newsDto: NewsDto,
    private val newsDao: NewsDao,
    private val schedulerProvider: SchedulerProvider
) : NewsRepository {

    private val TAG = javaClass.simpleName

    private val disposables = CompositeDisposable()
    private fun launch(job: () -> Disposable) = disposables.add(job())

    override fun onClear() = disposables.clear()
    override fun getNews(): Observable<List<NewsEntity>> = newsDao.getAll()

    override suspend fun getNewsById(id: Long): NewsEntity = newsDao.get(id)

    override fun refreshNews() {
        refreshAllNews()
    }

    private fun refreshAllNews() {
        launch {
            newsDto.getNews().with(schedulerProvider).subscribe({
                Completable.fromAction {
                    newsDao.deleteAll()
                }.with(schedulerProvider)
                    .doOnComplete {
                        Completable.fromAction {
                            newsDao.insertAll(it.results.map { f ->
                                NewsEntity(
                                    f.id, f.url, f.byline, f.title, f.date,
                                    f.media?.get(0)?.mediaMetaData?.get(0)?.url
                                )
                            })
                        }.with(schedulerProvider)
                            .subscribe()
                    }
                    .subscribe()
            }, {
                handleError(it)
            })
        }
    }

    private fun handleError(error: Throwable) {
        if (error is HttpException) {
            try {
                val message = error.response().errorBody()?.string()
                if (message != null && message.isNotEmpty()) {
                    Log.d(TAG, message)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        error.printStackTrace()
    }

}