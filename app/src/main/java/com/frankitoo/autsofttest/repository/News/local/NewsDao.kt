package com.frankitoo.autsofttest.repository.News.local

import androidx.room.*
import io.reactivex.Observable

@Dao
interface NewsDao {

    @Query("SELECT * from newsTable WHERE id = :id")
    suspend fun get(id: Long): NewsEntity

    @Query("SELECT * from newsTable")
    fun getAll(): Observable<List<NewsEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(newsEntity:  List<NewsEntity>)

    @Update
    fun update(newsEntity: List<NewsEntity>)

    @Query("DELETE from newsTable")
    fun deleteAll()
}