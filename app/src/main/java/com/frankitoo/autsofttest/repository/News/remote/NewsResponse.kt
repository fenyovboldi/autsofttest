package com.frankitoo.autsofttest.repository.News.remote

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class NewsResponse(
    @SerializedName("status")
    @Expose var status: String? = null,
    @SerializedName("results")
    @Expose var results: List<NewsModel>
)