package com.frankitoo.autsofttest

import android.app.Application
import com.frankitoo.autsofttest.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        val appModule = listOf(rxModule, newsModule)

        startKoin {
            androidLogger()
            androidContext(this@MainApplication)
            modules(appModule + remoteSourceModule + localSourceModule + stringResourceModule)
        }
    }
}