package com.frankitoo.autsofttest

import com.frankitoo.autsofttest.di.*
import com.frankitoo.autsofttest.repository.StringResource
import io.mockk.every
import io.mockk.mockk
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject

class MyKoinTest : KoinTest {

    private val stringResourceTest: StringResource by inject()
    private val application = mockk<MainApplication>()

    @Before
    fun before() {
        startKoin {
            androidContext(application)
            modules(newsModule + rxModule + localSourceModule + remoteSourceModule + stringResourceModule)
        }
    }

    @After
    fun after() {
        stopKoin()
    }

    @Test
    fun stringResTest() {
        val expectedResult = "MyString"
        every {
            application.getString(R.string.my_string)
        } returns expectedResult
        val testString = stringResourceTest.getStringFromActivity()

        println("Koin test run")

        assert(testString == expectedResult)
    }

}
