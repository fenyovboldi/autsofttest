package com.frankitoo.autsofttest

import com.frankitoo.autsofttest.di.*
import io.mockk.mockk
import org.junit.After
import org.junit.Test
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.check.checkModules

class CheckModulesTest : KoinTest {

    private val application = mockk<MainApplication>()

    @Test
    fun checkModules() {
        startKoin {
            androidContext(application)
            modules(newsModule + rxModule + localSourceModule + remoteSourceModule + stringResourceModule)
        }.checkModules()
    }

    @After
    fun close(){
        stopKoin()
    }
}